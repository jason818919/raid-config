# **Introduction**
------
Using RAID 0, RAID 1 or RAID 10 to make your computer more reliable.

# **Important**
------
Need to root.
Or, you can install the RAID with the debiain linux. Then, you do not need the command below.
Please check the drives status oftenly.
```
$ cat /proc/mdstat
```

# **Usage**
------
Firstly, observe your drives
```
$ df -h
$ lsblk
$ blkid
```
Then, using gdisk to partition your drives to fit in your RAID.
Note that using fd00 as Linux RAID type.
```
$ gdisk /dev/sdb
```
Install software RAID program
```
$ apt-get install mdadm
```
Create 'md' RAID device
```
$ mdadm --create /dev/md0 --level=10 --raid-device=4 /dev/sd[a-d]1
```
Observe the detail of md device
```
$ mdadm --detail /dev/md0
```
Create a filesystem on md device
```
$ mkfs.ext4 /dev/md0
```
Mount on your directory
```
$ mount /dev/md0 /home
```
Edit the /etc/fstab, get the md0 uuid and add.
```
$ vim /etc/fstab
```
Finally, reboot the system and see what happening.
```
$ sync
$ reboot
```

# **Good Tutorial**
------
http://www.tecmint.com/create-raid-10-in-linux/

# **Author**

Jason Chen

